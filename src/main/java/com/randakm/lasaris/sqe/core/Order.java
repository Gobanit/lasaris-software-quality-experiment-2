package com.randakm.lasaris.sqe.core;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/***
 * Class representing the order from eshop.
 *
 */
public class Order implements Serializable {
  private static final long serialVersionUID = -4243439034175729322L;

  @CsvBindByName(column = "order_id")
  private String id;
  @CsvBindByName(column = "order_date")
  @CsvDate(value = "dd.MM.YYYY")
  private Date date;
  @CsvBindByName(column = "customer_email")
  private String customerEmail;
  @CsvBindByName(column = "customer_address")
  private String customerAddress;
  @CsvBindByName(column = "total_price")
  private BigDecimal totalPrice;
  @CsvBindByName(column = "order_status")
  private OrderStatus status;

  /**
   * Getter for id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Setter for id.
   *
   * @param id - the id to set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Getter for date.
   *
   * @return the date
   */
  public Date getDate() {
    return date;
  }

  /**
   * Setter for date.
   *
   * @param date - the date to set
   */
  public void setDate(Date date) {
    this.date = date;
  }

  /**
   * Getter for customerEmail.
   *
   * @return the customerEmail
   */
  public String getCustomerEmail() {
    return customerEmail;
  }

  /**
   * Setter for customerEmail.
   *
   * @param customerEmail - the customerEmail to set
   */
  public void setCustomerEmail(String customerEmail) {
    this.customerEmail = customerEmail;
  }

  /**
   * Getter for customerAddress.
   *
   * @return the customerAddress
   */
  public String getCustomerAddress() {
    return customerAddress;
  }

  /**
   * Setter for customerAddress.
   *
   * @param customerAddress - the customerAddress to set
   */
  public void setCustomerAddress(String customerAddress) {
    this.customerAddress = customerAddress;
  }

  /**
   * Getter for totalPrice.
   *
   * @return the totalPrice
   */
  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  /**
   * Setter for totalPrice.
   *
   * @param totalPrice - the totalPrice to set
   */
  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  /**
   * Getter for status.
   *
   * @return the status
   */
  public OrderStatus getStatus() {
    return status;
  }

  /**
   * Setter for status.
   *
   * @param status - the status to set
   */
  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "Order [id=" + id + ", date=" + date + ", customerEmail=" + customerEmail + ", customerAddress="
        + customerAddress + ", totalPrice=" + totalPrice + ", status=" + status + "]";
  }

}
