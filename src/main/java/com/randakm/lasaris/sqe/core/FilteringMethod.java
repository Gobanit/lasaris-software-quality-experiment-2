package com.randakm.lasaris.sqe.core;

import java.util.List;

/**
 * Manipulation method for filtering order list.
 *
 */
abstract public class FilteringMethod implements ManipulationMethod {

  /**
   * Filters orders, the result list is subset of input orders.
   * 
   * @param orders - orders to filter
   * @return subset of input orders
   */
  abstract protected List<Order> filter(List<Order> orders);

  public List<Order> execute(List<Order> orders, AnalyticsResult analyticsResult) {
    return filter(orders);
  }
}
