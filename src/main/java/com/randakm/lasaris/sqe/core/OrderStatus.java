package com.randakm.lasaris.sqe.core;

/**
 * Enumeration of all possible order status.
 *
 */
public enum OrderStatus {
  UNPAID, PAID
}
