package com.randakm.lasaris.sqe.core;

import java.util.List;

/**
 * Interface representing method tu manipulate the order dataset.
 * 
 */
public interface ManipulationMethod {

  /**
   * 
   * @param orders          - list of orders
   * @param analyticsResult - object for storing analytics results
   * @return list of orders, might be modified
   */
  public List<Order> execute(List<Order> orders, AnalyticsResult analyticsResult);
}
