package com.randakm.lasaris.sqe.core;

import java.io.Serializable;

/**
 * Class representing single analyzis of order list.
 *
 */
abstract public class AnalyticsResultItem implements Serializable {
  private static final long serialVersionUID = 4191937104293006686L;

  /**
   * returns human-readable name/description of the analytics result item
   * 
   * @return result item name
   */
  abstract public String getName();
}
