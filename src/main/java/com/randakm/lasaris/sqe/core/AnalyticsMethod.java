package com.randakm.lasaris.sqe.core;

import java.util.List;

/**
 * Manipulation method for analytics of order list.
 *
 */
abstract public class AnalyticsMethod implements ManipulationMethod {

  /**
   * Analyzes the orders of the input and creates analytics result items.
   * 
   * @param orders - orders to analyze.
   * @return list of analytics result items.
   */
  abstract protected List<AnalyticsResultItem> analyze(List<Order> orders);

  public List<Order> execute(List<Order> orders, AnalyticsResult analyticsResult) {
    this.analyze(orders).forEach((item) -> analyticsResult.addItem(item));
    return orders;
  }

}
