package com.randakm.lasaris.sqe;

import com.randakm.lasaris.sqe.core.AnalyticsResult;
import com.randakm.lasaris.sqe.core.ManipulationMethod;
import com.randakm.lasaris.sqe.core.Order;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Class responsible for applying the manipulation methods to input orders and
 * generating the {@link AnalyticsResult}.
 *
 */
@Component
public class ExperimentLogicCore {
  private static final Logger LOGGER = LoggerFactory.getLogger(ExperimentLogicCore.class);

  /**
   * Applies the {@link ManipulationMethod} chain on the order list and generates
   * the analytics result.
   * 
   * @param orders       - list of orders
   * @param methodsChain - chaing of methods to apply
   * @return analytics result
   */
  public AnalyticsResult executeManipulationMethods(List<Order> orders, List<ManipulationMethod> methodsChain) {
    AnalyticsResult analyticsResult = new AnalyticsResult();
    for (ManipulationMethod mm : methodsChain) {
      LOGGER.info("Apply method " + mm.getClass());
      orders = mm.execute(orders, analyticsResult);
      LOGGER.info("Current number of orders: " + orders.size());
    }

    return analyticsResult;
  }
}
