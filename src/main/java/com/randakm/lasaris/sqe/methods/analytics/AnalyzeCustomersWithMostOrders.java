package com.randakm.lasaris.sqe.methods.analytics;

import com.randakm.lasaris.sqe.core.AnalyticsMethod;
import com.randakm.lasaris.sqe.core.AnalyticsResultItem;
import com.randakm.lasaris.sqe.core.Order;
import com.randakm.lasaris.sqe.methods.analytics.data.CustomerAndNumber;
import com.randakm.lasaris.sqe.methods.analytics.data.CustomersWithMostOrders;

import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

/**
 * Analytics method returning N (by default 3) customers with most orders in the
 * dataset.
 *
 */
@Component("AnalyzeCustomersWithMostOrders")
public class AnalyzeCustomersWithMostOrders extends AnalyticsMethod {
  public static final int DEFAULT_CUSTOMER_COUNT = 3;

  private final int customerCount;

  public AnalyzeCustomersWithMostOrders() {
    this(DEFAULT_CUSTOMER_COUNT);
  }

  public AnalyzeCustomersWithMostOrders(int customerCount) {
    this.customerCount = customerCount;
  }

  @Override
  protected List<AnalyticsResultItem> analyze(List<Order> orders) {
    Set<Entry<String, Long>> set = orders.stream().map((o) -> o.getCustomerEmail()).filter((e) -> e != null)
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting())).entrySet();

    CustomersWithMostOrders cwmo = new CustomersWithMostOrders();

    set.stream().sorted(Entry.comparingByKey()).sorted(Collections.reverseOrder(Entry.comparingByValue()))
        .limit(customerCount).forEachOrdered((e) -> {
          CustomerAndNumber can = new CustomerAndNumber();
          can.setCustomerEmail(e.getKey());
          can.setNumberOfOrders(e.getValue());
          cwmo.getCustomers().add(can);
        });

    return Collections.singletonList(cwmo);
  }

}
