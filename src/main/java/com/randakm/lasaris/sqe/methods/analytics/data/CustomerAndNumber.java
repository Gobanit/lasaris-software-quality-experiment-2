package com.randakm.lasaris.sqe.methods.analytics.data;

import java.io.Serializable;

/**
 * Wrapper class for customer email and number of his orders.
 *
 */
public class CustomerAndNumber implements Serializable {
  private static final long serialVersionUID = 1001229851952257651L;

  private String customerEmail;
  private Long numberOfOrders;

  /**
   * Getter for customerEmail.
   *
   * @return the customerEmail
   */
  public String getCustomerEmail() {
    return customerEmail;
  }

  /**
   * Setter for customerEmail.
   *
   * @param customerEmail - the customerEmail to set
   */
  public void setCustomerEmail(String customerEmail) {
    this.customerEmail = customerEmail;
  }

  /**
   * Getter for numberOfOrders.
   *
   * @return the numberOfOrders
   */
  public Long getNumberOfOrders() {
    return numberOfOrders;
  }

  /**
   * Setter for numberOfOrders.
   *
   * @param numberOfOrders - the numberOfOrders to set
   */
  public void setNumberOfOrders(Long numberOfOrders) {
    this.numberOfOrders = numberOfOrders;
  }

}
