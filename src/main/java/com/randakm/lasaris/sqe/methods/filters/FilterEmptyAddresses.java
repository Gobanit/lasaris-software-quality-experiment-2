package com.randakm.lasaris.sqe.methods.filters;

import com.randakm.lasaris.sqe.core.Order;
import com.randakm.lasaris.sqe.utils.HelperMethods;

import org.springframework.stereotype.Component;

/**
 * Filter method, removing all orders with missing customer address.
 *
 */
@Component("FilterEmptyAddresses")
public class FilterEmptyAddresses extends SimpleAbstractFilter {

  @Override
  boolean shouldKeep(Order o) {
    return !HelperMethods.isEmpty(o.getCustomerAddress());
  }

}
