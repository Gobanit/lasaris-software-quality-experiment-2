package com.randakm.lasaris.sqe.methods.analytics.data;

import com.randakm.lasaris.sqe.core.AnalyticsResultItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Analytics result item containing total prices of paid orders status per year.
 *
 */
public class TotalYearOrderPrices extends AnalyticsResultItem {
  private static final long serialVersionUID = 4747358828593526203L;

  private List<YearAndPrice> totalPrices = new ArrayList<>();

  @Override
  public String getName() {
    return "Total prices per year for paid orders";
  }

  /**
   * Getter for totalPrices.
   *
   * @return the totalPrices
   */
  public List<YearAndPrice> getTotalPrices() {
    return totalPrices;
  }

  /**
   * Setter for totalPrices.
   *
   * @param totalPrices - the totalPrices to set
   */
  public void setTotalPrices(List<YearAndPrice> totalPrices) {
    this.totalPrices = totalPrices;
  }

  
}
