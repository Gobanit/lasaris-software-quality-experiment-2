package com.randakm.lasaris.sqe.methods.analytics.data;

/**
 * Wrapper class for year and price.
 *
 */
public class YearAndPrice {
  private Integer year;
  private Double price;

  /**
   * Getter for year.
   *
   * @return the year
   */
  public Integer getYear() {
    return year;
  }

  /**
   * Setter for year.
   *
   * @param year - the year to set
   */
  public void setYear(Integer year) {
    this.year = year;
  }

  /**
   * Getter for price.
   *
   * @return the price
   */
  public Double getPrice() {
    return price;
  }

  /**
   * Setter for price.
   *
   * @param price - the price to set
   */
  public void setPrice(Double price) {
    this.price = price;
  }

}
