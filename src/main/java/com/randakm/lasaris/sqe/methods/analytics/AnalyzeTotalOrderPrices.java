package com.randakm.lasaris.sqe.methods.analytics;

import com.randakm.lasaris.sqe.core.AnalyticsMethod;
import com.randakm.lasaris.sqe.core.AnalyticsResultItem;
import com.randakm.lasaris.sqe.core.Order;
import com.randakm.lasaris.sqe.core.OrderStatus;
import com.randakm.lasaris.sqe.methods.analytics.data.TotalYearOrderPrices;
import com.randakm.lasaris.sqe.methods.analytics.data.YearAndPrice;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

/**
 * Analytics methods finding the total prices of {@link OrderStatus#PAID} orders
 * per year.
 *
 */
@Component("AnalyzeTotalOrderPrices")
public class AnalyzeTotalOrderPrices extends AnalyticsMethod {

  @Override
  protected List<AnalyticsResultItem> analyze(List<Order> orders) {
    TotalYearOrderPrices ayop = new TotalYearOrderPrices();

    orders.stream().filter((o) -> o.getStatus() == OrderStatus.PAID).collect(Collectors.groupingBy((o) -> {
      Calendar cal = Calendar.getInstance();
      cal.setTime(o.getDate());
      return cal.get(Calendar.YEAR);
    })).forEach((year, list) -> {
      double sum = list.stream().collect(Collectors.summingDouble((o) -> o.getTotalPrice().doubleValue()));
      sum = Math.round(sum * 100) / 100.0;

      YearAndPrice yaap = new YearAndPrice();
      yaap.setYear(year);
      yaap.setPrice(sum);
      ayop.getTotalPrices().add(yaap);
    });

    ayop.getTotalPrices().sort((a,b) -> a.getYear().compareTo(b.getYear()));
    return Collections.singletonList(ayop);
  }

}
