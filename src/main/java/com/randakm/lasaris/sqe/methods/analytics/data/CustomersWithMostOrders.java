package com.randakm.lasaris.sqe.methods.analytics.data;

import com.randakm.lasaris.sqe.core.AnalyticsResultItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Analytics result item containing list of customers with most orders.
 *
 */
public class CustomersWithMostOrders extends AnalyticsResultItem {
  private static final long serialVersionUID = 5697704253341223112L;

  private List<CustomerAndNumber> customers = new ArrayList<>();

  @Override
  public String getName() {
    return "Customers with highest number of paid orders";
  }

  /**
   * Getter for customers.
   *
   * @return the customers
   */
  public List<CustomerAndNumber> getCustomers() {
    return customers;
  }

  /**
   * Setter for customers.
   *
   * @param customers - the customers to set
   */
  public void setCustomers(List<CustomerAndNumber> customers) {
    this.customers = customers;
  }

}
