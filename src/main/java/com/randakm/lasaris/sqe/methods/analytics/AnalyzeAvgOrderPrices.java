package com.randakm.lasaris.sqe.methods.analytics;

import com.randakm.lasaris.sqe.core.AnalyticsMethod;
import com.randakm.lasaris.sqe.core.AnalyticsResultItem;
import com.randakm.lasaris.sqe.core.Order;
import com.randakm.lasaris.sqe.core.OrderStatus;
import com.randakm.lasaris.sqe.methods.analytics.data.AvgYearOrderPrices;
import com.randakm.lasaris.sqe.methods.analytics.data.YearAndPrice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

/**
 * Analytics method analyzing average order prices per year. It creates two
 * analytics result items - one for {@link OrderStatus#PAID} and one for
 * {@link OrderStatus#UNPAID} orders.
 *
 */
@Component("AnalyzeAvgOrderPrices")
public class AnalyzeAvgOrderPrices extends AnalyticsMethod {

  @Override
  protected List<AnalyticsResultItem> analyze(List<Order> orders) {
    List<AnalyticsResultItem> results = new ArrayList<>();
    results.add(analyze(orders, OrderStatus.PAID));
    results.add(analyze(orders, OrderStatus.UNPAID));
    return results;
  }

  private AnalyticsResultItem analyze(List<Order> orders, OrderStatus status) {
    AvgYearOrderPrices ayop = new AvgYearOrderPrices();
    ayop.setStatus(status);

    orders.stream().filter((o) -> o.getStatus() == status).collect(Collectors.groupingBy((o) -> {
      Calendar cal = Calendar.getInstance();
      cal.setTime(o.getDate());
      return cal.get(Calendar.YEAR);
    })).forEach((year, list) -> {
      double avg = list.stream().collect(Collectors.averagingDouble((o) -> o.getTotalPrice().doubleValue()));
      avg = Math.round(avg * 100) / 100.0;

      YearAndPrice yaap = new YearAndPrice();
      yaap.setYear(year);
      yaap.setPrice(avg);
      ayop.getAvgPrices().add(yaap);
    });

    ayop.getAvgPrices().sort((a,b) -> a.getYear().compareTo(b.getYear()));
    return ayop;
  }

}
