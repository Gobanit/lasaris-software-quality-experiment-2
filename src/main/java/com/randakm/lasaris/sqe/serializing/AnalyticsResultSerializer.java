package com.randakm.lasaris.sqe.serializing;

import com.randakm.lasaris.sqe.OutputFormat;
import com.randakm.lasaris.sqe.core.AnalyticsResult;

/**
 * Interface for serializing {@link AnalyticsResult} to some text format.
 *
 */
public interface AnalyticsResultSerializer {

  /**
   * Serialize {@link AnalyticsResult} to string.
   * 
   * @param result object to serialize
   * @return text representation
   */
  public String serialize(AnalyticsResult result);

  /**
   * Method to find which {@link OutputFormat} is supported by this class.
   * 
   * @return supported {@link OutputFormat}
   */
  public OutputFormat getSupportedFormat();

}
