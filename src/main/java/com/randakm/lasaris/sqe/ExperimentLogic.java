package com.randakm.lasaris.sqe;

import com.randakm.lasaris.sqe.core.AnalyticsResult;
import com.randakm.lasaris.sqe.core.ManipulationMethod;
import com.randakm.lasaris.sqe.core.Order;
import com.randakm.lasaris.sqe.serializing.AnalyticsResultSerializer;
import com.randakm.lasaris.sqe.serializing.JsonAnalyticsResultSerializer;
import com.randakm.lasaris.sqe.serializing.ReflectionAnalyticsResultSerializer;
import com.randakm.lasaris.sqe.serializing.XMLAnalyticsResultSerializer;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Class responsible for loading the data, applying the manipulation methods
 * through {@link ExperimentLogicCore} and serializing the results.
 *
 */
@Component
public class ExperimentLogic {
  private static final Logger LOGGER = LoggerFactory.getLogger(ExperimentLogic.class);

  private ExperimentLogicCore core;

  /**
   * Creates class instance.
   * 
   * @param core - logic core
   */
  @Autowired
  public ExperimentLogic(ExperimentLogicCore core) {
    this.core = core;
  }

  /**
   * Loads data from dataset, applies the manipulation methods and serialize the
   * result to specified file in specified format.
   * 
   * @param dataSetUrl     - url of the input dataset
   * @param outputFilePath - file path, where the serialized result should be
   *                       written
   * @param ouputFormat    - desired output format
   * @param methodsChain   - list of manipulation methods to apply
   * @throws IOException - in case of input read or output write failure
   */
  public void execute(String dataSetUrl, String outputFilePath, OutputFormat ouputFormat,
      List<ManipulationMethod> methodsChain) throws IOException {
    AnalyticsResultSerializer serializer = chooseSerializer(ouputFormat);

    // load data
    List<Order> orders = loadDataset(dataSetUrl);

    AnalyticsResult analyticsResult = core.executeManipulationMethods(orders, methodsChain);

    // generate output
    String output = serializer.serialize(analyticsResult);
    LOGGER.info("Output: \n" + output);

    LOGGER.info("Writing output to " + new File(outputFilePath).getAbsolutePath());
    FileUtils.writeStringToFile(new File(outputFilePath), output, "UTF-8");
  }

  private AnalyticsResultSerializer chooseSerializer(OutputFormat outputFormat) {
    switch (outputFormat) {
      case JSON:
        return new JsonAnalyticsResultSerializer();
      case PLAIN:
        return new ReflectionAnalyticsResultSerializer();
      case XML:
        return new XMLAnalyticsResultSerializer();
      default:
        throw new IllegalArgumentException("No serializer available for " + outputFormat + " format yet");
    }
  }

  private List<Order> loadDataset(String dataSetUrl) throws MalformedURLException, IOException {
    InputCSVParser parser = new InputCSVParser();
    List<Order> orders = parser.loadFromUrl(dataSetUrl);
    LOGGER.info("Total number of orders: " + orders.size());
    for (Order o : orders) {
      LOGGER.debug("" + o);
    }

    return orders;
  }

}
