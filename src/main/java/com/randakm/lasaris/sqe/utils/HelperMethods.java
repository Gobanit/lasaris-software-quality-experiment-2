package com.randakm.lasaris.sqe.utils;

public class HelperMethods {

  public static boolean isEmpty(String s) {
    return s == null || s.isEmpty();
  }
}
