package com.randakm.lasaris.sqe;

import com.randakm.lasaris.sqe.core.ManipulationMethod;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Main class, starting the spring boot and also the command line endpoint,
 * executes the main logic through {@link ExperimentLogic} class.
 *
 */
@SpringBootApplication
public class SoftwareQualityExperimentApplication implements CommandLineRunner {
  private static final Logger LOGGER = LoggerFactory.getLogger(SoftwareQualityExperimentApplication.class);

  @Autowired
  private ApplicationContext appContext;
  
  @Autowired
  private ExperimentLogic experimentLogic;

  public static void main(String[] args) throws IOException, ParseException {
    SpringApplication.run(SoftwareQualityExperimentApplication.class, args);
  }

  /**
   * Starts the command line application
   */
  @Override
  public void run(String... args) throws Exception {
    CommandLine cmd = configureCmd(args);
    run(cmd);
  }

  private CommandLine configureCmd(String[] args) throws ParseException {
    Options options = new Options();

    Option o = new Option("d", "path to remote or local CSV file.");
    o.setArgs(1);
    o.setRequired(true);
    options.addOption(o);

    o = new Option("m", "list of methods (filter/analytics) to apply");
    o.setArgs(Option.UNLIMITED_VALUES);
    o.setRequired(true);
    options.addOption(o);

    o = new Option("o", "output format and path to file");
    o.setArgs(2);
    o.setRequired(true);
    options.addOption(o);

    CommandLineParser clparser = new DefaultParser();
    CommandLine cmd = clparser.parse(options, args);
    return cmd;
  }

  private void run(CommandLine cmd) throws MalformedURLException, IOException, ParseException, ClassNotFoundException {
    // print arguments
    LOGGER.info("d value: " + cmd.getOptionValue("d"));
    LOGGER.info("m value: " + Arrays.deepToString(cmd.getOptionValues("m")));
    LOGGER.info("o value: " + Arrays.deepToString(cmd.getOptionValues("o")));

    // parse arguments
    String dataSetUrl = cmd.getOptionValue("d");
    OutputFormat outputFormat = OutputFormat.valueOf(cmd.getOptionValues("o")[0].toUpperCase());
    String outputFilePath = cmd.getOptionValues("o")[1];

    // configure to-be used methods
    List<ManipulationMethod> methodsChain = new ArrayList<>();

    String[] methods = cmd.getOptionValues("m");
    for (String m : methods) {
      if (!appContext.containsBean(m)) {
        throw new IllegalArgumentException("No manipulation method '" + m + "' is defined");
      }
      ;
      ManipulationMethod mm = appContext.getBean(m, ManipulationMethod.class);
      methodsChain.add(mm);
    }

    // executing the logic
    experimentLogic.execute(dataSetUrl, outputFilePath, outputFormat, methodsChain);
  }

}
