package com.randakm.lasaris.sqe.filters;

import com.randakm.lasaris.sqe.TestHelperUtils;
import com.randakm.lasaris.sqe.core.AnalyticsResult;
import com.randakm.lasaris.sqe.core.Order;
import com.randakm.lasaris.sqe.methods.filters.FilterEmptyAddresses;
import com.randakm.lasaris.sqe.methods.filters.FilterEmptyEmails;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class testing individual filter methods on test sample data
 *
 */
@RunWith(SpringRunner.class)
public class FiltersTest {
 
  private FilterEmptyEmails emptyEmailFilter;
  private FilterEmptyAddresses emptyAddressFilter;
  private List<Order> orders;
  
  @Before
  public void init() {
    emptyEmailFilter = new FilterEmptyEmails();
    emptyAddressFilter = new FilterEmptyAddresses();
    
    orders = TestHelperUtils.orderSample();
  }
  
  
  
  @Test
  public void testEmptyEmailsFilter() {
    List<Order> filtered = emptyEmailFilter.execute(orders, new AnalyticsResult());
    
    Assert.assertEquals(5, filtered.size());
  }
  
  @Test
  public void testEmptyAddressFilter() {
    List<Order> filtered = emptyAddressFilter.execute(orders, new AnalyticsResult());
    
    Assert.assertEquals(4, filtered.size());
  }
}