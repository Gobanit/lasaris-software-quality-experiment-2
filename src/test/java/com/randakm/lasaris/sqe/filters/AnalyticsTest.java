package com.randakm.lasaris.sqe.filters;

import com.randakm.lasaris.sqe.TestHelperUtils;
import com.randakm.lasaris.sqe.core.AnalyticsResult;
import com.randakm.lasaris.sqe.core.Order;
import com.randakm.lasaris.sqe.core.OrderStatus;
import com.randakm.lasaris.sqe.methods.analytics.AnalyzeAvgOrderPrices;
import com.randakm.lasaris.sqe.methods.analytics.AnalyzeCustomersWithMostOrders;
import com.randakm.lasaris.sqe.methods.analytics.AnalyzeTotalOrderPrices;
import com.randakm.lasaris.sqe.methods.analytics.data.AvgYearOrderPrices;
import com.randakm.lasaris.sqe.methods.analytics.data.CustomersWithMostOrders;
import com.randakm.lasaris.sqe.methods.analytics.data.TotalYearOrderPrices;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class testing individual analytics methods on test sample data
 *
 */
@RunWith(SpringRunner.class)
public class AnalyticsTest {
  private AnalyzeTotalOrderPrices totalOrdePricesAnalytics;
  private AnalyzeCustomersWithMostOrders customersWithMostOrdersAnalytics;
  private AnalyzeAvgOrderPrices avgOrderPricesAnalytics;

  private List<Order> orders;

  @Before
  public void init() {
    totalOrdePricesAnalytics = new AnalyzeTotalOrderPrices();
    customersWithMostOrdersAnalytics = new AnalyzeCustomersWithMostOrders();
    avgOrderPricesAnalytics = new AnalyzeAvgOrderPrices();
    orders = TestHelperUtils.orderSample();
  }

  @Test
  public void testTotalOrderPricesAnalytics() {
    AnalyticsResult res = new AnalyticsResult();

    totalOrdePricesAnalytics.execute(orders, res);

    List<TotalYearOrderPrices> items = TestHelperUtils.filterAnalyticsResult(TotalYearOrderPrices.class, res);

    Assert.assertEquals(1, items.size());
    TotalYearOrderPrices tyop = items.get(0);

    Assert.assertEquals(2, tyop.getTotalPrices().size()); // only paid status

    tyop.getTotalPrices().sort((a, b) -> {
      return a.getYear().compareTo(b.getYear());
    });

    double delta = 0.001;
    Assert.assertEquals(new Integer(2015), tyop.getTotalPrices().get(0).getYear());
    Assert.assertEquals(10.3, tyop.getTotalPrices().get(0).getPrice().doubleValue(), delta);
    Assert.assertEquals(new Integer(2016), tyop.getTotalPrices().get(1).getYear());
    Assert.assertEquals(4613.1, tyop.getTotalPrices().get(1).getPrice().doubleValue(), delta);
  }
  
  @Test
  public void testCustomersWithMostOrdersAnalytics() {
    AnalyticsResult res = new AnalyticsResult();

    customersWithMostOrdersAnalytics.execute(orders, res);

    List<CustomersWithMostOrders> items = TestHelperUtils.filterAnalyticsResult(CustomersWithMostOrders.class, res);

    Assert.assertEquals(1, items.size());
    CustomersWithMostOrders cwmo = items.get(0);
    
    Assert.assertEquals(3, cwmo.getCustomers().size());    
    
    // expecting customers with equal number of orders to be sorted lexicographically.    
    Assert.assertEquals("xy@pobox.sk", cwmo.getCustomers().get(0).getCustomerEmail());    
    Assert.assertEquals(2, cwmo.getCustomers().get(0).getNumberOfOrders().longValue());    
    Assert.assertEquals("", cwmo.getCustomers().get(1).getCustomerEmail());
    Assert.assertEquals(1, cwmo.getCustomers().get(1).getNumberOfOrders().longValue());    
    Assert.assertEquals(" ", cwmo.getCustomers().get(2).getCustomerEmail());
    Assert.assertEquals(1, cwmo.getCustomers().get(2).getNumberOfOrders().longValue());
  }
  
  @Test
  public void testAvgOrderPricesAnalytics() {
    AnalyticsResult res = new AnalyticsResult();

    avgOrderPricesAnalytics.execute(orders, res);

    List<AvgYearOrderPrices> items = TestHelperUtils.filterAnalyticsResult(AvgYearOrderPrices.class, res);

    Assert.assertEquals(2, items.size());
    items.sort((a,b) -> a.getStatus().compareTo(b.getStatus()));
    
    double delta = 0.001;
    
    AvgYearOrderPrices tyopPaid = items.get(0);
    Assert.assertEquals(OrderStatus.UNPAID, tyopPaid.getStatus());
    Assert.assertEquals(2, tyopPaid.getAvgPrices().size());    
    Assert.assertEquals(2015, tyopPaid.getAvgPrices().get(0).getYear().intValue());
    Assert.assertEquals(0.11, tyopPaid.getAvgPrices().get(0).getPrice().doubleValue(), delta);
    Assert.assertEquals(2017, tyopPaid.getAvgPrices().get(1).getYear().intValue());
    Assert.assertEquals(10, tyopPaid.getAvgPrices().get(1).getPrice().doubleValue(), delta);


    tyopPaid = items.get(1);
    Assert.assertEquals(OrderStatus.PAID, tyopPaid.getStatus());
    Assert.assertEquals(2, tyopPaid.getAvgPrices().size());  
    Assert.assertEquals(2015, tyopPaid.getAvgPrices().get(0).getYear().intValue());
    Assert.assertEquals(10.30, tyopPaid.getAvgPrices().get(0).getPrice().doubleValue(), delta);
    Assert.assertEquals(2016, tyopPaid.getAvgPrices().get(1).getYear().intValue());
    Assert.assertEquals(1153.28, tyopPaid.getAvgPrices().get(1).getPrice().doubleValue(), delta);
  }

}
