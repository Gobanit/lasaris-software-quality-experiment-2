package com.randakm.lasaris.sqe;

import com.randakm.lasaris.sqe.core.AnalyticsMethod;
import com.randakm.lasaris.sqe.core.AnalyticsResult;
import com.randakm.lasaris.sqe.core.AnalyticsResultItem;
import com.randakm.lasaris.sqe.core.FilteringMethod;
import com.randakm.lasaris.sqe.core.ManipulationMethod;
import com.randakm.lasaris.sqe.core.Order;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class testing the {@link ExperimentLogicCore} functionality.
 *
 */
@RunWith(SpringRunner.class)
public class ExperimentLogicCoreTest {
  private FilteringMethod filter1;
  private AnalyticsMethod analytics1;
  private AnalyticsMethod analytics2;

  private List<Order> orders;

  private ExperimentLogicCore logicCore = new ExperimentLogicCore();

  @Before
  public void init() {

    filter1 = Mockito.mock(FilteringMethod.class);
    analytics1 = Mockito.mock(AnalyticsMethod.class);
    analytics2 = Mockito.mock(AnalyticsMethod.class);

    orders = TestHelperUtils.orderSample();
  }

  @Test
  public void testEmptyChain() {
    List<ManipulationMethod> chain = new ArrayList<>();

    AnalyticsResult ar = logicCore.executeManipulationMethods(orders, chain);
    Assert.assertNotNull(ar);
    Assert.assertTrue(ar.getAnalyticsItems().isEmpty());
  }

  @Test
  public void testFilteringApplies() {
    List<ManipulationMethod> chain = new ArrayList<>();
    chain.add(filter1);
    chain.add(analytics1);

    
    Mockito.when(filter1.execute(Mockito.any(), Mockito.any())).thenReturn(orders.subList(0, 2));
    
    AnalyticsResult ar = logicCore.executeManipulationMethods(orders, chain);
    Assert.assertNotNull(ar);

    Mockito.verify(filter1, Mockito.times(1)).execute(Mockito.eq(orders), Mockito.any());
    Mockito.verify(analytics1).execute(Mockito.argThat((list) -> list.size() == 2), Mockito.any());
  }
  
  @Test
  public void testAnalyticsResultIsPropagated() {
    List<ManipulationMethod> chain = new ArrayList<>();
    chain.add(analytics1);
    chain.add(analytics2);
    
    AnalyticsResultItem item1 = Mockito.mock(AnalyticsResultItem.class);
    AnalyticsResultItem item2 = Mockito.mock(AnalyticsResultItem.class);
    AnalyticsResultItem item3 = Mockito.mock(AnalyticsResultItem.class);
        
    Mockito.when(analytics1.execute(Mockito.any(), Mockito.any())).then((inv) -> {
      AnalyticsResult ar = inv.getArgument(1, AnalyticsResult.class);
      ar.addItem(item1);
      ar.addItem(item2);
      return inv.getArgument(0);
    });
    Mockito.when(analytics2.execute(Mockito.any(), Mockito.any())).then((inv) -> {
      AnalyticsResult ar = inv.getArgument(1, AnalyticsResult.class);
      ar.addItem(item3);
      return inv.getArgument(0);
    });
  

    AnalyticsResult result = logicCore.executeManipulationMethods(orders, chain);
    
    Assert.assertEquals(3, result.getAnalyticsItems().size());
    Assert.assertTrue(result.getAnalyticsItems().contains(item1));
    Assert.assertTrue(result.getAnalyticsItems().contains(item2));
    Assert.assertTrue(result.getAnalyticsItems().contains(item3));    
  }

}
