package com.randakm.lasaris.sqe;

import com.randakm.lasaris.sqe.core.AnalyticsResult;
import com.randakm.lasaris.sqe.core.AnalyticsResultItem;
import com.randakm.lasaris.sqe.core.Order;
import com.randakm.lasaris.sqe.core.OrderStatus;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Class providing usefull methods for tests.
 *
 */
public class TestHelperUtils {
  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-mm-dd");
  
  /**
   * Creates order based on specified parameters
   */
  public static Order createOrder(long id, String email, String address, OrderStatus status, String date, String price) {
    Order o = new Order();
    o.setId("Order#"+id);
    o.setCustomerEmail(email);
    o.setCustomerAddress(address);
    
    try {
      o.setDate(DATE_FORMAT.parse(date));
    } catch (NullPointerException | ParseException e) {
      o.setDate(null);
      e.printStackTrace();
    }
    o.setStatus(status);
    o.setTotalPrice(new BigDecimal(price));
    
    return o;
  }
  
  /**
   * Generates short sample of orders
   */
  public static List<Order> orderSample() {
    List<Order> orders = new ArrayList<>();
    
    orders.add(createOrder(1, "abc@gmail.com", "Sedmokraskova", OrderStatus.PAID, "2015-12-14", "10.30"));
    orders.add(createOrder(2, "", "Pupavova", OrderStatus.PAID, "2016-01-02", "1267.2"));
    orders.add(createOrder(3, "xy@pobox.sk", "  ", OrderStatus.UNPAID, "2015-05-13", "0.11"));
    orders.add(createOrder(4, null, "Mileticova", OrderStatus.PAID, "2016-02-05", "1550.20"));
    orders.add(createOrder(5, " ", null, OrderStatus.PAID, "2016-01-05", "1550.20"));
    orders.add(createOrder(6, "xy@pobox.sk", null, OrderStatus.PAID, "2016-09-25", "245.5"));
    orders.add(createOrder(7, "abc", "", OrderStatus.UNPAID, "2017-10-11", "10"));
    
    return orders;    
  }
  
  
  /**
   * Filters {@link AnalyticsResultItem} in {@link AnalyticsResult} by class.
   */
  @SuppressWarnings("unchecked")
  public static <T extends AnalyticsResultItem> List<T> filterAnalyticsResult(Class<T> cls, AnalyticsResult result) {
    List<T> items = new ArrayList<>();
    for (AnalyticsResultItem i : result.getAnalyticsItems()) {
      if (cls.isAssignableFrom(i.getClass())) {
        items.add((T) i);
      }
    }

    return items;
  }

}
