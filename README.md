# Lasaris Software Quality Experiment

## How to run
1. build project using maven - run command in root directory `mvn clean install`
2. move to `target` subfolder
3. rename **software-quality-experiment-0.0.1-SNAPSHOT.jar** and copy it to desired location, or just leave the jar as it is
4. run the jar with desired arguments, for example:
`java -jar .\software-quality-experiment-0.0.1-SNAPSHOT.jar -d https://is.muni.cz/el/fi/jaro2020/PV260/um/seminars/java_groups/initial_assignment/orders_data.csv -m FilterEmptyAddresses AnalyzeAvgOrderPrices AnalyzeCustomersWithMostOrders -o json ./output.json` The method names are the same as the bean components names. Currently implemented are:
    * **FilterEmptyAddresses**
    * **FilterEmptyEmails**
    * **AnalyzeAvgOrderPrices**
    * **AnalyzeCustomersWithMostOrders**
    * **AnalyzeTotalOrderPrices**
